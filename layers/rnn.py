import numpy as np
from numpy.random import randn

from .layer import Layer


class RNN(Layer):
    def __init__(self, input_size, hidden_size):
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.Wxh = np.random.randn(hidden_size, input_size) * 0.01
        self.Whh = np.random.randn(hidden_size, hidden_size) * 0.01
        self.bh = np.zeros((hidden_size, 1))

    def forward(self, input_data):
        h = np.zeros((self.hidden_size, input_data.shape[1]))
        self.input_data = input_data
        self.hiddens = {}
        for t in range(input_data.shape[0]):
            x = input_data[t].reshape(-1, 1)
            self.hiddens[t] = np.tanh(np.dot(self.Wxh, x) + np.dot(self.Whh, h) + self.bh)
            h = self.hiddens[t]
        return h

    def backward(self, d_output, learning_rate):
        dWxh = np.zeros_like(self.Wxh)
        dWhh = np.zeros_like(self.Whh)
        dbh = np.zeros_like(self.bh)
        dh_next = np.zeros_like(self.hiddens[0])
        for t in reversed(range(len(self.input_data))):
            dh = d_output if t == len(self.input_data) - 1 else d_output + dh_next
            dh_raw = (1 - self.hiddens[t] ** 2) * dh
            dWxh += np.dot(dh_raw, self.input_data[t].reshape(1, -1))
            dWhh += np.dot(dh_raw, self.hiddens[t-1].T)
            dbh += dh_raw
            dh_next = np.dot(self.Whh.T, dh_raw)
        self.Wxh -= learning_rate * dWxh
        self.Whh -= learning_rate * dWhh
        self.bh -= learning_rate * dbh
        return dh_next