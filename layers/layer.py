from abc import abstractmethod, ABC

import numpy as np


class Layer(ABC):
    @abstractmethod
    def forward(self, input_data: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def backward(self, d_output: np.ndarray, learning_rate) -> np.ndarray:
        pass
