import os
import pickle

import pandas as pd
from ucimlrepo import fetch_ucirepo

DATASET_PATH = './task3.pickle'


def task3_loader():
    if not os.path.exists(DATASET_PATH):

        print("Downloading task3")
        steel_industry_energy_consumption = fetch_ucirepo(id=851)
        x = steel_industry_energy_consumption.data.features
        y = steel_industry_energy_consumption.data.targets
        df = pd.concat([x, y], axis=1)

        df.to_pickle(DATASET_PATH)

    return pd.read_pickle(DATASET_PATH)


