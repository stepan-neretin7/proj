from activations.relu import ReLU
from layers.dense import DenseLayer
from activations.sigmoid import SigmoidActivation
from losses.mse import MeanSquaredError
from mlp import MLP
import numpy as np

layers = [
    DenseLayer(2, 2),
    ReLU(),
    DenseLayer(2, 1),
]

model = MLP(layers, MeanSquaredError())

x = np.array([[1, 2], [3, 3], [4, 4], [0, 1], [10, 10]])
y = np.array([[5], [9], [12], [2], [30]])

model.train(x, y, epochs=10000, learning_rate=0.001)
res = model.predict(np.array([2, 2]))
print(res)
